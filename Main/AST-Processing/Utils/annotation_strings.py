# <--- Code snippets to be inserted for code instrumentation --->

saveRuntimeCode = """
with open(r'../Main/AST-Processing/Utils/Dynamic-Annotation/runtimes.txt', 'w') as fp:
    for item in runtimes:
        item = str(item)
        fp.write('%s\\n' % item)
"""

fileDataTypeInput = """
try:
    datatypes.append([{id}, 'i', '{input}', type('{input}')])
except:
    datatypes.append([{id}, 'i', '{input}', ''])
"""

variableDataTypeInput = """
try:
    datatypes.append([{id}, 'i', '{input}', type({input})])
except:
    datatypes.append([{id}, 'i', '{input}', ''])
"""

fileDataTypeOutput = """
try:
    datatypes.append([{id}, 'o', '{output}', type('{output}')])
except:
    datatypes.append([{id}, 'o', '{output}', ''])
"""

variableDataTypeOutput = """
try:
    datatypes.append([{id}, 'o', '{output}', type({output})])
except:
    datatypes.append([{id}, 'o', '{output}', ''])
"""

saveDataTypeCode = """
with open(r'../Main/AST-Processing/Utils/Dynamic-Annotation/datatypes.txt', 'w') as fp:
    for item in datatypes:
        item = str(item)
        fp.write('%s\\n' % item)
"""

saveDataSizeCode = """
with open(r'../Main/AST-Processing/Utils/Dynamic-Annotation/datasizes.txt', 'w') as fp:
    for item in datasizes:
        item = str(item)
        fp.write('%s\\n' % item)
"""

variableDataSizeInput = "datasizes.append([{id}, 'i', '{input}', sys.getsizeof({input})])"

fileDataSizeInput = "datasizes.append([{id}, 'i', '{input}', sys.getsizeof('{input}')])"

variableDataSizeOutput = "datasizes.append([{id}, 'o', '{output}', sys.getsizeof({output})])"

fileDataSizeOutput = "datasizes.append([{id}, 'o', '{output}', sys.getsizeof('{output}')])"