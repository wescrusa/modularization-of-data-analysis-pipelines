import sys
import pickle
from sklearn.ensemble import RandomForestClassifier

if __name__ == '__main__':

    
    clf = RandomForestClassifier()
    clf_4=clf
    
    params = {'n_estimators': [4, 6, 9],
                          'max_features': ['log2', 'sqrt','auto'],
                          'criterion': ['entropy', 'gini'],
                          'max_depth': [2, 3, 5, 10],
                          'min_samples_split': [2, 3, 5],
                          'min_samples_leaf': [1,5,8]
                         }
    params_4=params
    with open(snakemake.output[0], 'wb') as out_file:
        pickle.dump(params_4, out_file)
    with open(snakemake.output[1], 'wb') as out_file:
        pickle.dump(clf_4, out_file)
