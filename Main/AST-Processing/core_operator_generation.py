import ast
import sys
from Utils.file_utils import save_dict_to_file

class CoreOperators:
    """
    The CoreOperators class holds the source code, as well as some basic static information extracted from the AST of our source code.
    Operators are represented using dictionaries, in which essential information for later rule extraction is saved.

    We define two categories of dictionaries: a full dictionary containing all code artefacts, and one with only "relevant" code artefacts.
    Relevant code artefacts are artefacts we want to translate to Snakemake rules.
    """
    
    def __init__(self, filename):
        self.sourceCode = open(filename).read()
        self.tree = ast.parse(self.sourceCode)
        self.dictionaries = []
        self.relevantDictionaries = []

    # <--- functions for displaying operator information --->


    def display_dictionaries(self, kind="dictionaries"):
        if kind == "dictionaries":
            nodes = self.dictionaries
        else:
            nodes = self.relevantDictionaries

        for node in nodes:
            text = """{{\nid: {id}\ntype: {type}\ninput: {input}\noutput: {output}\ndependencies: {dependencies}\ncode: \n{code}\nruntime: {runtime}\n}}\n"""
            print(text.format(id=node["id"], type=node["type"], input=node["input"], output=node["output"], \
                dependencies=node["dependencies"], code=node["code"], runtime=node["runtime"]))

    # <--- functions for building operator dictionaries using static AST information --->

    def extract_type(self, node):
        return type(node).__name__

    def extract_code(self, node):
        code = ast.get_source_segment(self.sourceCode, node, padded=False)
        return code

    def build_dictionaries(self):
        """
        Collects static information from our AST and saves it in a list of dictionaries.
        """
        self.dictionaries = list()
        for i in range(0, len(self.tree.body)):
            node = self.tree.body[i]

            d = {"id":0, "type":"", "input":[], "output":[], "dependencies":[], "code":"", "runtime":0}
            d["id"] = i
            d["type"] = self.extract_type(node)
            d["code"] = self.extract_code(node)

            self.dictionaries.append(d)

    def build_relevantDictionaries(self):
        """
        Collect only dictionaries relevant for rule extraction.
        """
        self.relevantDictionaries = list()
        for d in self.dictionaries:
            if d["type"] == "Assign" or d["type"] == "Expr" or d["type"] == "For" or d["type"] == "While" or d["type"] == "If" or d["type"] == "AugAssign":
                self.relevantDictionaries.append(d)

    def save_dictionaries(self):
        """
        Saves dictionaries to flat and indented file format.
        """
        file = "core_dictionaries.json"
        data = self.dictionaries
        save_dict_to_file(data, file)

    def save_relevant_dictionaries(self):
        """
        Saves relevant dictionaries to flat and indented file format.
        """
        file = "relevant_dictionaries.json"
        data = self.relevantDictionaries
        save_dict_to_file(data, file)

# <--- Main --->

def main():
    ops = CoreOperators(sys.argv[1])   

    print("\n<--- AST generated for file {filename} --->\n".format(filename=sys.argv[1]))

    print("\n<--- Operators as dictionaries over all code artefacts --->\n")
    ops.build_dictionaries()
    ops.display_dictionaries()
    
    print("\n<--- Operators as dictionaries over relevant code artefacts --->\n")
    ops.build_relevantDictionaries()
    ops.display_dictionaries("relevantDictionaries")

    print("\n<--- Saving dictionaries --->\n")
    ops.save_dictionaries()
    ops.save_relevant_dictionaries()
    

if __name__ == "__main__":
    main()