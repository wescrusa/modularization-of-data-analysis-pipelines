# !!!
# The data for this script was taken from a Kaggle tutorial,
# it can be found at: https://www.kaggle.com/code/mmetter/pokemon-data-analysis-tutorial/data
# !!!

import numpy as np 
import pandas as pd 
import matplotlib.pyplot as plt
import seaborn as sns  

data = pd.read_csv('pokemon.csv')

ghostDF = data[data['against_ghost']>=1.0]

ghostDF["against_ghost"].apply(np.cumsum)

ghostDF.to_csv("ghostPokemon.csv")

def save_correlation_map(data):
    f,ax = plt.subplots(figsize=(18, 18))
    sns.heatmap(data.corr(), annot=True, linewidths=.5, fmt= '.1f',ax=ax)
    plt.savefig("ghost.png")

save_correlation_map(ghostDF)

for index, row in data.iterrows():
    row['against_electric'] += 0.1

data.to_csv("updatedPokemon.csv")
    
a = np.arange(100)

for i in range(a.shape[0]-1):
    a[i] = a[i+1]

pd.DataFrame(a).to_csv("array.csv")
