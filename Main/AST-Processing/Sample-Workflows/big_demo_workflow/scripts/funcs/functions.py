
def P_drop(data,drop_strategies):
    for column, strategy in drop_strategies.items():
                data=data.drop(labels=[column], axis=strategy)
    return data

import numpy as np

def D_fill_arrays():
    a1 = np.array([1, 2, 3, 4, 5])
    a2 = np.zeros(10000)
    np.append(a1, 6)
    np.append(a1, 7)
    np.append(a1, 8)
    a2[0] = 1
    a2[1] = 2
    for i in range(0,len(a2)):
                a2[i] = 1
    return a1, a2

import globals

def O__get_train_and_test():
    globals.O_X_train=globals.O_all_data[:globals.O_number_of_train]
    globals.O_X_test=globals.O_all_data[globals.O_number_of_train:]

import globals
import pandas as pd

def O__get_all_data():
    return pd.concat([globals.O_train, globals.O_test])

import globals
import pandas as pd

def G_save_result():
    Submission = pd.DataFrame({'PassengerId': globals.G_submission_id,
                                               'Survived': globals.G_Y_pred})
    file_name="{}_{}.csv".format(globals.G_strategy_type,globals.G_current_clf_name.lower())
    Submission.to_csv(file_name, index=False)
    print("Submission saved file name: ",file_name)

import globals

def D_increment1(i):
    globals.D_dummy_array1[i] = 1 + globals.D_dummy_array1[i]

import globals
import numpy as np

def D_initialize_arrays():
    a1, a2 = D_fill_arrays()
    globals.D_dummy_array1 = a1
    globals.D_dummy_array2 = a2

import globals

def D_increment2(i):
    globals.D_dummy_array2[i] = 1 + globals.D_dummy_array2[i]

