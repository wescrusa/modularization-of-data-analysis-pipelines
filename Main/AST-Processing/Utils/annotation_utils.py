# <--- functions to collect information from lists as strings --->

def collect_time(l):
    """
    Return id and runtime stored in string of form '[id, time]'.
    """
    id = int(l.split(',')[0][1:])
    time = float(l.split(',')[1][:-2])
    return id, time

def collect_datatypes(l):
    """ 
    Return id, data name and datatype/module name of string '[id, io, name, type.__name__']. 
    """
    id = int(l.split(',')[0][1:])
    io = l.split(',')[1][2:-1]
    name = l.split(',')[2][2:-1]
    typ = l.split(',')[3].split('\'')[1]
    datatype = typ.split('.')[-1]
    library = typ.split('.')[0]
    return id, io, name, datatype, library

def collect_datasize(l):
    """ 
    Return id, data name and datasize of string '[id, io, name, size]'. 
    """
    id = int(l.split(',')[0][1:])
    io = l.split(',')[1][2:-1]
    name = l.split(',')[2][2:-1]
    size = int(l.split(',')[3][:-2])
    return id, io, name, size