from operator_generation import Operators
from Utils.function_utils import is_file
from Utils.file_utils import get_foldername
from Utils.annotation_strings import *
from Utils.annotation_utils import collect_time, collect_datatypes, collect_datasize
import sys
import os

class AnnotatedOperators(Operators):
    """
    AnnotatedOperators extends Operators with dynamic information collection. 
    We extract runtime of artefacts, datatypes of inputs and outputs, and new dependencies concluded from datatypes using automatic code instrumentation. 
    """

    # <--- functions for initialising AnnotatedOperators --->

    # build dictionaries using operator_generation functions
    def build_operators(self):
        print("> Building operators with static information from AST...")
        self.get_function_handlers()
        self.process_relevant_artefacts()
        self.remove_funcNames()
        self.redirect_import_dependencies()
        self.remove_duplicates()
        self.sticky_artefacts()

    # get caller arguments
    def get_caller_args(self, args):
        for arg in args:
            self.callerArgs = self.callerArgs + " " + str(arg)

    def __init__(self, filename):
        super().__init__(filename)
        self.build_operators()
        self.folderName = get_foldername(filename)
        self.callerArgs = ""

    # <--- code instrumentation for runtime --->

    def create_runtime_file(self):
        """ 
        Instruments source code in file runtime.py which collects runtime for each artefact. 
        """
        code = "import time\n"
        code += "runtimes = list()\n"
        # change source code
        for d in self.dictionaries:
            code += "start_time = time.time()\n"
            code += d["code"] 
            code = code + "\n" + "runtimes.append([{id}, time.time() - start_time])\n".format(id=d["id"])
        code += saveRuntimeCode
        with open(r'Utils/Dynamic-Annotation/runtime.py', 'w') as fp:
            fp.write(code)

    def collect_runtimes(self):
        """ 
        Reads runtimes from txt and saves them to relevant dictionaries. 
        """
        runtimes = dict()
        with open('Utils/Dynamic-Annotation/runtimes.txt') as f:
            for line in f:
                id, time = collect_time(line)
                runtimes[id] = time
        for d in self.relevantDictionaries:
            d["runtime"] = runtimes[d["id"]]

    # <--- code instrumentation for datatypes --->

    def rewrite_code_datatypes(self):
        """ 
        Instruments source code to collect datatypes for each artefact.  
        """
        code = ""
        code += "datatypes = list()\n"
        # change source code
        for node in self.dictionaries:
            # add a lines collecting datatype of inputs
            for input in node["input"]:
                if is_file(input):
                    code = code + fileDataTypeInput.format(id = node["id"], input=input) + "\n"
                else:
                    code = code + variableDataTypeInput.format(id = node["id"], input=input) + "\n"
            # collect our original code lines
            code += node["code"] 
            # add a line to stop the timer and save the time
            code = code + "\n" 
            for output in node["output"]:
                if is_file(output):
                    code = code + fileDataTypeOutput.format(id = node["id"], output=output) + "\n"
                else:
                    code = code + variableDataTypeOutput.format(id = node["id"], output=output) + "\n"
        return code 

    def create_datatype_file(self):
        """ 
        Saves instrumented code in datatype.py. 
        """
        code = self.rewrite_code_datatypes()
        code += saveDataTypeCode
        with open(r'Utils/Dynamic-Annotation/datatype.py', 'w') as fp:
            fp.write(code)

    def get_deps_from_datatype(self, datatype):
        """
        Collects module dependency from dynamic datatype information.
        """
        # datatype[0] := actual datatype
        # datatype[1] := module name of datatype
        dep1 = self.find_import(datatype[0])
        dep2 = self.find_import(datatype[1])
        return [dep1, dep2]

    def extract_datatypes(self, datatypes):
        """ 
        Saves datatypes to dictionaries and adds dependencies if necessary. 
        """
        for d in self.relevantDictionaries:
            id = d["id"]
            for i in range(0,len(d["input"])):
                input = d["input"][i]
                if is_file(input):
                    datatypes[(id, input, 'i')][0] = "file"
                d["input"][i] = [d["input"][i], datatypes[(id, input, 'i')][0]]

                # add any new dependencies we can deduce from datatype
                deps = self.get_deps_from_datatype(datatypes[(id, input, 'i')])
                for dep in [x for x in deps if x is not None and x not in d["dependencies"]]:
                    d["dependencies"].append(dep)

            for i in range(0,len(d["output"])):
                output = d["output"][i]
                if is_file(output):
                    datatypes[(id, output, 'o')][0] = "file"
                d["output"][i] = [d["output"][i], datatypes[(id, output, 'o')][0]]

                # add any new dependencies we can deduce from datatype
                deps = self.get_deps_from_datatype(datatypes[(id, output, 'o')])
                for dep in [x for x in deps if x is not None and x not in d["dependencies"]]:
                    d["dependencies"].append(dep)

    def collect_datatypes(self):
        """ 
        Reads from datatypes.txt and calls extraction to dictionaries. 
        """
        # reading runtimes from txt
        datatypes = dict()
        with open('Utils/Dynamic-Annotation/datatypes.txt') as f:
            for line in f:
                id, io, name, datatype, library = collect_datatypes(line)
                datatypes[(id, name, io)] = [datatype, library]
        # saving in dictionaries
        self.extract_datatypes(datatypes)

    # <--- code instrumentation for datasize --->
    
    def rewrite_code_datasizes(self):
        """
        Instruments source code to collect datasizes for each artefact. 
        """
        code = "import sys \n"
        code += "datasizes = list()\n"
        # change source code
        for node in self.dictionaries:
            # add a lines collecting datatype of inputs
            for input in node["input"]:
                if is_file(input):
                    code = code + fileDataSizeInput.format(id=node["id"], input=input) + "\n"
                else:
                    code = code + variableDataSizeInput.format(id=node["id"], input=input) + "\n"
            # collect our original code lines
            code += node["code"] 
            code = code + "\n" 
            for output in node["output"]:
                if is_file(output):
                    code = code + fileDataSizeOutput.format(id=node["id"], output=output) + "\n"
                else:
                    code = code + variableDataSizeOutput.format(id=node["id"], output=output) + "\n"
                    
        code += saveDataSizeCode
                    
        return code

    def create_datasize_file(self):
        """
        Saves instrumented code to datasize.py.
        """
        code = self.rewrite_code_datasizes()
        with open(r'Utils/Dynamic-Annotation/datasize.py', 'w') as fp:
            fp.write(code)
    
    def extract_datasizes(self, datasizes):
        """
        Adds datasize to each input and output of artefact.
        """
        for d in self.relevantDictionaries:
            id = d["id"]
            for i in range(0,len(d["input"])):
                input = d["input"][i][0]
                d["input"][i].append(datasizes[(id, input, 'i')])
            for i in range(0,len(d["output"])):
                output = d["output"][i][0]
                d["output"][i].append(datasizes[(id, output, 'o')])

    def collect_datasizes(self):
        """ 
        Reads from datasizes.txt and calls extraction to dictionaries. 
        """
        datasizes = dict()
        with open('Utils/Dynamic-Annotation/datasizes.txt') as f:
            for line in f:
                id, io, name, size = collect_datasize(line)
                datasizes[(id, name, io)] = size
        # saving in dictionaries
        self.extract_datasizes(datasizes)


    # <--- execute script to collect dynamic information --->

    def run_annotator(self):
        # Note: runs script three times, each with different annotations
        command = "cd Utils/Dynamic-Annotation; ./run_annotator.sh {folderName} {args}".format(folderName=self.folderName, args=self.callerArgs)
        os.system(command)

    # <--- refine data dependencies --->

    def discard_special_types(self):
        """
        Wrongful data dependencies can be corrected by analyzing the type of a variable.
        For example: in the code snippet 'type(int)', int is recognized as an input variable.
        However, 'int' was never created beforehand -> inadequate data dependency.
        Here, we discard such built-in type inputs/outputs.
        """
        for d in self.relevantDictionaries:
            toDiscard = list()
            for i in range(0, len(d["input"])):
                if d["input"][i][1] == "type":
                    toDiscard.append(d["input"][i])
            for item in toDiscard:
                d["input"].remove(item)

# <--- Main --->

def main():
    print("<<< COLLECTION OF OPERATORS USING DYNAMIC INFORMATION >>>\n\n")
    ops = AnnotatedOperators(sys.argv[1])
    ops.get_caller_args(sys.argv[2:])
    
    print("Instrumenting code in file Utils/Dynamic-Annotation/runtime.py...")
    ops.create_runtime_file()
    print("Instrumenting code in file Utils/Dynamic-Annotation/datatype.py...")
    ops.create_datatype_file()
    print("Instrumenting code in file Utils/Dynamic-Annotation/datasize.py...")
    ops.create_datasize_file()
    print("Running Utils/Dynamic-Annotation/runtime.py, datatype.py and datasize.py...")
    ops.run_annotator()
    print("Extracting runtimes, datatypes, and datasizes...")
    ops.collect_runtimes()
    ops.collect_datatypes()
    ops.collect_datasizes()

    ops.discard_special_types()

    print("\n<--- Operators as dictionaries over relevant code artefacts --->\n")
    ops.display_dictionaries("relevantDictionaries")

if __name__ == "__main__":
    main()
