# modularization-of-data-analysis-pipelines



## Contact

- Annabell Maja Westphal (HU Berlin)
    - Contact: wescrusa@outlook.de

## Installation

### Linux
- Clone repository
- Check if python 3.9 is installed: `python3.9 --version`
    - If it is not already installed:
    ```
    sudo apt update
    sudo apt install python3.9
    sudo  apt-get install python3.9-dev python3.9-venv
    ```
- Check if virtualenv is installed: `virtualenv --version`
    - If it is not already installed: `pip install virtualenv`
- In folder 'modularization-of-data-analysis-pipelines', create and activate python 3.9 environment: 
    ```
    python3.9 -m venv venv
    source venv/bin/activate
    ```
- Install requirements: `pip install -r requirements.txt`
- Install requirements necessary for demo execution:
```
cd Demo # or BigDemo
pip install -r requirements.txt
``` 

## Project Explanation & Example Usage

We aim to modularize python scripts by means of semi-automatically extracting a [Snakemake Workflow](https://snakemake.readthedocs.io/en/stable/).

#### Input

We offer a simple **demo python script** `test.py`, which reads data from `pokemon.csv` and executes a couple of pandas, numpy, matplotlib, and seaborn expressions on said data. This script can be found in the `Demo` directory.
- If users wish to execute this project on our demo case, just replace `filename` with `../../Demo/test.py` in the sample usages below. E.g.: call `python operator_generation.py ../../Demo/test.py`.

Another script to test our approach can be found in the `BigDemo` directory. In this demo script, more complex data science steps are executed. Both scripts are adapted from Kaggle projects - please refer to the header comment in each script for more information.

If users wish to extract a workflow from their **own python scripts**, they must first make sure that all modules used within the scripts are installed in the python 3.9 environment. This is necessary as scripts have to be executed for dynamic information collection later on. Next up, we propose users should create a folder that contains the python scripts to be modularized and all necessary files they need under the core directory `modularization-of-data-analysis-pipelines`. E.g.: Create a folder called `MyScripts` containing `script.py`.
- Follow further instructions by replacing `filename` with `../../MyScripts/script.py`.

### Usage

Make sure the python 3.9 environment is activated before executing any of our scripts and switch to the right directory. 
```
cd Main/AST-Processing
```
Each module in the `Main/AST-Processing` folder can be run on its own to display intermediate extraction results. We will explain each script usage and output below.
If users are not interested in intermediate results, they can skip to section [3. Workflow Extraction](#3-workflow-extraction).

#### 1. AST Processing
*First, we build operator dictionaries using the AST of our source code.*

- Executing `python core_operator_generation.py [filename]` yields a display of the core artefacts/operators from our source code as dictionaries built by extracting some simple information from the AST. These artefacts pose as a basis for our static analysis (next step) and may be inspected in the `JSON` directory.

- Executing `python operator_generation.py [filename]` performs an extended def-use analysis on our test code. The script call displays AST code artefacts used for iteration, import names and their alias, as well as function calls and function definition analysis results. The final operators resulting from our static analysis are stored in the `JSON` directory.

- Executing `python operator_annotation.py [filename]` annotates our static operators with further dynamic information necessary for workflow modularization.
    - Internally, this call creates three new instrumented scripts `runtime.py`, `datatype.py`, and `datasize.py` in the folder `Utils/Dynamic-Annotation`. 
    - Within this folder one can also find a shell script called `run_annotation.sh`, which was executed by the above call automatically to generate `runtimes.txt`, `datatypes.txt`, and `datasizes.txt`.

#### 2. Modularization
*Next, we build a graph from our operators and modularize this graph by combining graph nodes following special criteria.*

- Executing `python graph_generation.py [filename]` preprocesses no-output dictionaries, renames dictionaries to display data changing through operations, and creates a networkx graph from these dictionaries.
    - The graph is saved in the directory `Graphs` under the name `startingGraph.png`

- Executing `python graph_modularization.py [filename]` displays our graph after default modularization, as well as weak components within this graph.
    - The resulting graph can be found in the `Graphs` directory under the name `finalGraph.png`
    - In this step, users can also pass arguments to change the modularization parameters (similarity weights, static metrics, or iteration number). An example call is given below. 
```
# Run topological clustering for 5 iterations with weights w=[1,2,1,1]
python graph_modularization.py ../../Demo/test.py -i 5 -t "topological" -w 1 2 1 1
```
- Additionally, a JSON file containing operator dictionaries can be passed to this script to simplify the testing of the modularization algorithms with synthetic examples. If no such file is passed as an argument, this script runs all prior analysis scripts before the modularization process.
    - For more information, users can call `python graph_modularization.py [filename] -h`.

#### 3. Workflow Extraction
*Lastly, we save operators as Snakemake rules and create executable scripts from our source code.*

- Executing `python workflow_generation.py [filename]` runs all above steps and extracts the modularized graph to a Snakemake workflow.
    - The final workflow is saved in a new directory called `workflow`.
    - Because this script internally calls methods provided by `graph_modularization.py`, users may pass arguments as described above.

## Notebooks
In the `Experiments` directory, users can find two experimental Jupyter Notebooks. In `Decapsulation.ipynb` we provide documented functions to transform object-oriented scripts into procedural scripts that can be used as input for our framework. In `Experiments.ipynb` we provide plotting functions to compare the results of different modularization parameters.