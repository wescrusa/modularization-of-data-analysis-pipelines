# <--- functions for topological ordering --->

import networkx as nx

def get_order(dictionaries):
    """
    Returns list of node ids in order of presence in source code.
    """
    order = list()
    for d in dictionaries:
        order.append(d["id"])
    return order

def get_random_order(Graph):
    """
    Returns depth-first topological order.
    """
    return list(nx.topological_sort(Graph))

def update_order(order, index, val):
    """
    Track which nodes in topological order have been merged.
    """
    oldVal = order[index+1]
    i = index+1
    while i < len(order) and order[i] == oldVal:
        order[i] = val
        i += 1
    return order

# <--- maximum similarity --->

def find_max_sim_top(simMatrix):
    """
    Find maximum similarity in list, smaller than limit
    """
    maxSim = 0
    index = -1
    for i in range(0, len(simMatrix)):
        sim = simMatrix[i]
        if sim >= maxSim:
            maxSim = sim
            index = i
    return index, maxSim