import sys
import pickle
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
from funcs.functions import save_correlation_map

if __name__ == '__main__':

    with open(snakemake.input[0], 'rb') as in_file:
        ghostDF_1 = pickle.load(in_file)
    
    save_correlation_map(ghostDF_1)
