import sys
import pickle
import globals
import pandas as pd

if __name__ == '__main__':

    with open(snakemake.input[0], 'rb') as in_file:
        globals.S_dats_8 = pickle.load(in_file)
    with open(snakemake.input[1], 'rb') as in_file:
        min_lengtht = pickle.load(in_file)
    
    title_names = (globals.S_dats_8['Title'].value_counts() < min_lengtht)
    
    globals.S_dats_8['Title'] = globals.S_dats_8['Title'].apply(lambda x: 'Misc' if title_names.loc[x] == True else x)
    globals.S_dats_9=globals.S_dats_8
    
    globals.S_dats_9 = globals.S_dats_9
    globals.S_dats_10=globals.S_dats_9
    
    globals.S_dats_10['FareBin'] = pd.qcut(globals.S_dats_10['Fare'], 4)
    globals.S_dats_11=globals.S_dats_10
    
    globals.S_dats_11['AgeBin'] = pd.cut(globals.S_dats_11['Age'].astype(int), 5)
    globals.S_dats_12=globals.S_dats_11
    
    drop_strategy = {'Age': 1,  
                        'Name': 1,
                        'Fare': 1}
    drop_strategy_1=drop_strategy
    with open(snakemake.output[0], 'wb') as out_file:
        pickle.dump(drop_strategy_1, out_file)
    with open(snakemake.output[1], 'wb') as out_file:
        pickle.dump(globals.S_dats_12, out_file)
