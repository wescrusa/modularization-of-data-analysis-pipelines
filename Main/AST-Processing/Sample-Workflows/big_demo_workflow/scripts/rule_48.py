import sys
import pickle
import globals
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier

if __name__ == '__main__':

    with open(snakemake.input[0], 'rb') as in_file:
        globals.G_clf_and_params_2 = pickle.load(in_file)
    
    clf = SVC()
    clf_2=clf
    
    params = [ {'C': [1, 10, 100, 1000], 'kernel': ['linear']},
                               {'C': [1, 10, 100, 1000], 'gamma': [0.001, 0.0001], 'kernel': ['rbf']}]
    params_2=params
    
    globals.G_clf_and_params_2.append((clf_2, params_2))
    globals.G_clf_and_params_3=globals.G_clf_and_params_2
    
    clf=DecisionTreeClassifier()
    clf_3=clf
    
    params={'max_features': ['auto', 'sqrt', 'log2'],
                      'min_samples_split': [2,3,4,5,6,7,8,9,10,11,12,13,14,15],
                      'min_samples_leaf':[1],
                      'random_state':[123]}
    params_3=params
    
    globals.G_clf_and_params_3.append((clf_3,params_3))
    globals.G_clf_and_params_4=globals.G_clf_and_params_3
    with open(snakemake.output[0], 'wb') as out_file:
        pickle.dump(globals.G_clf_and_params_4, out_file)
