rule rule_4:
    input: 'data/pokemon.csv'
    output: 'data/updatedPokemon.csv', 'data/ghostDF.pkl'
    script: '../scripts/rule_4.py'

rule rule_6:
    input: 'data/a_1.pkl', 'data/ghostDF.pkl'
    output: 'data/array.csv', 'data/ghostPokemon.csv', 'data/ghostDF_1.pkl'
    script: '../scripts/rule_6.py'

rule rule_9:
    input: 'data/ghostDF_1.pkl'
    output: 'data/ghost.png'
    script: '../scripts/rule_9.py'

rule rule_12:
    input: 
    output: 'data/a_1.pkl'
    script: '../scripts/rule_12.py'

