import sys
import pickle
import numpy as np
import pandas as pd

if __name__ == '__main__':

    with open(snakemake.input[0], 'rb') as in_file:
        a_1 = pickle.load(in_file)
    with open(snakemake.input[1], 'rb') as in_file:
        ghostDF = pickle.load(in_file)
    
    ghostDF["against_ghost"].apply(np.cumsum)
    ghostDF_1=ghostDF
    
    pd.DataFrame(a_1).to_csv(snakemake.output[0])
    
    ghostDF_1.to_csv(snakemake.output[1])
    with open(snakemake.output[2], 'wb') as out_file:
        pickle.dump(ghostDF_1, out_file)
