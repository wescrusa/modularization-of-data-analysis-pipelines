# <--- functions for replacing variable names --->

import re

def rename_code(code, prevName, newName):
    """
    Replaces only whole word occurences of prevName with newName in code.
    """
    newCode = re.sub(r"\b{}\b".format(prevName), newName, code)
    return newCode

def add_code(code, prevName, newName):
    """
    Adds assignment to track new output name has been created in code.
    """
    newCode = code +"\n"
    newCode = newCode + newName + "=" + prevName
    return newCode