# <--- functions to handle node merging within graph --->

from . import function_utils

# <--- graph preparation --->

def get_successor_nodes(Graph, parentID):
    """
    Gets successor node dictionaries.
    """
    successorIDs = list(Graph.successors(parentID))
    successorNodes = [Graph.nodes[ID] for ID in successorIDs]
    return successorNodes

def get_node_inputs(nodes):
    """
    Returns list of all inputs from nodes.
    """
    inputs = list()
    for node in nodes:
        inputs += node["input"]
    return inputs

def delete_unnecessary_outputs(Graph):
    """
    Deletes all node outputs that are not needed by another node, and that are not files.
    """
    for ID in Graph.nodes:
        successors = get_successor_nodes(Graph, ID)
        successorsInputs = get_node_inputs(successors)
        for output in Graph.nodes[ID]["output"]:
            if not output in successorsInputs:
                if not function_utils.is_file(output[0]):
                    Graph.nodes[ID]["output"].remove(output)

def sum_run(Graph):
    """
    Sum over execution time of all Graph nodes.
    """
    run = 0
    for ID in Graph.nodes:
        run += Graph.nodes[ID]["runtime"]
    return run

def sum_size(Graph):
    """
    Sum over output size of all Graph nodes in byte.
    """
    size = 0
    for ID in Graph.nodes:
        for output in Graph.nodes[ID]["output"]:
            size += output[2]
    return size

def to_child(Graph, a, b):
    """
    Reassigns a and b so that b is child of a or None is returned.
    """
    # a is child of b
    if a in list(Graph.successors(b)):
        return b, a
    # b is child of a
    if b in list(Graph.successors(a)):
        return a, b
    # neither is child of other
    return None, None

# <--- merging two dictionaries --->

def get_needed_inputs(Graph, a, b):
    """
    When Merging nodes (a,b), we want to discard inputs that are either output of a or b.
    """
    nodeA = Graph.nodes[a]
    nodeB = Graph.nodes[b]
    
    needed_inputs = list()
    other_b_ins = [i for i in nodeB["input"] if i not in nodeA["output"]]
    other_a_ins = [i for i in nodeA["input"] if i not in nodeB["output"]]
    needed_inputs = other_b_ins
    for i in other_a_ins:
        if i not in needed_inputs:
            needed_inputs.append(i)
            
    return needed_inputs

def get_needed_outputs(Graph, a, b):
    """
    When Merging nodes (a,b), we only want to keep outputs that are still needed by other nodes.
    """
    still_needed_outputs = list()
    nodeA = Graph.nodes[a]
    nodeB = Graph.nodes[b]
    still_needed_outputs = [o for o in nodeA["output"] if function_utils.is_file(o[0])] + [o for o in nodeB["output"] if function_utils.is_file(o[0])]
    
    a_succs = list(Graph.successors(a))
    a_other_children = [child for child in a_succs if child != b]
    b_succs = list(Graph.successors(b))
    b_other_children = [child for child in b_succs if child != a]
    
    # collect the inputs of children of a and b
    possible_outputs = list()
    for succ in a_other_children + b_other_children:
        nodeSucc = Graph.nodes[succ]
        for i in nodeSucc["input"]:
            possible_outputs.append(i)
    
    # check if a or b contribute to the inputs collected above
    # if yes: these outputs should not be discarded while merging (a,b)
    for o in possible_outputs:
        if o in nodeA["output"] or o in nodeB["output"]:
            if o not in still_needed_outputs:
                still_needed_outputs.append(o)
                
    return still_needed_outputs

def merge_nodes_real(a,b, needed_inputs, needed_outputs):
    """
    Returns new node containing information from two original nodes.
    """
    node = {"id":[], "type":"", "input":[], "output":[], "dependencies":[], "code":"", "runtime":0}
    node["id"] = a["id"]
    #node["type"] = a["type"] + ", " + b["type"]
    node["input"] = needed_inputs
    node["output"] = needed_outputs
    node["dependencies"] = list(set(a["dependencies"] + b["dependencies"]))
    node["code"] = a["code"] + "\n\n" + b["code"]
    node["runtime"] = a["runtime"] + b["runtime"]
    
    return node

def merge_nodes(a,b):
    """
    Returns new node containing information from two original nodes.
    """
    node = {"id":[], "type":"", "input":[], "output":[], "dependencies":[], "code":"", "runtime":0}

    node["id"] = a["id"]
    node["type"] = a["type"] + ", " + b["type"]
    node["input"] = a["input"] + [i for i in b["input"] if i not in a["output"] and i not in a["input"]]
    node["output"] = b["output"] + [o for o in a["output"] if o not in b["output"]] 
    node["dependencies"] = list(set(a["dependencies"] + b["dependencies"]))
    node["code"] = a["code"] + "\n" + b["code"]
    node["runtime"] = a["runtime"] + b["runtime"]
    
    return node

# <--- Similarity --->

def find_max_sim(simMatrix):
    """
    Find maximum similarity lower than limit and index of this entry.
    """
    maxSim = 0
    maxI = -1
    maxJ = -1
    for i in range(0,len(simMatrix)):
        for j in range(i,len(simMatrix)):
            if simMatrix[i][j] >= maxSim:
                maxSim = simMatrix[i][j]
                maxI = i
                maxJ = j
    return maxSim, maxI, maxJ

# <--- node information --->

def get_node_LOC(dictionary):
    # LOC per node
    return len(dictionary["code"].split('\n'))