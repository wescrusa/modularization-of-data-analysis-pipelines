import sys
import pickle
import globals
import pandas as pd
from funcs.functions import P_drop

if __name__ == '__main__':

    with open(snakemake.input[0], 'rb') as in_file:
        globals.S_dats_1 = pickle.load(in_file)
    
    drop_strategy = {'PassengerId': 1,  
                                     'Cabin': 1,
                                     'Ticket': 1}
    
    globals.S_dats_1 = P_drop(globals.S_dats_1, drop_strategy)
    globals.S_dats_2=globals.S_dats_1
    
    fill_strategy = {'Age': 'Median',
                                     'Fare': 'Median',
                                     'Embarked': 'Mode'}
    
    for column, strategy in fill_strategy.items():
        if strategy == 'None':
            globals.S_dats_2[column] = globals.S_dats_2[column].fillna('None')
        elif strategy == 'Zero':
            globals.S_dats_2[column] = globals.S_dats_2[column].fillna(0)
        elif strategy == 'Mode':
            globals.S_dats_2[column] = globals.S_dats_2[column].fillna(globals.S_dats_2[column].mode()[0])
        elif strategy == 'Mean':
            globals.S_dats_2[column] = globals.S_dats_2[column].fillna(globals.S_dats_2[column].mean())
        elif strategy == 'Median':
            globals.S_dats_2[column] = globals.S_dats_2[column].fillna(globals.S_dats_2[column].median())
        else:
            print("{}: There is no such thing as preprocess strategy".format(strategy))
    fill_strategy_1=fill_strategy
    globals.S_dats_3=globals.S_dats_2
    with open(snakemake.output[0], 'wb') as out_file:
        pickle.dump(globals.S_dats_3, out_file)
