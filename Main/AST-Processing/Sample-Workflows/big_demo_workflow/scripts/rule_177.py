import sys
import pickle
import globals
import numpy as np
from funcs.functions import D_increment2, D_initialize_arrays, D_increment1, D_fill_arrays

if __name__ == '__main__':

    with open(snakemake.input[0], 'rb') as in_file:
        globals.G_results_5 = pickle.load(in_file)
    
    for clf_name, train_acc in globals.G_results_5.items():
              print("{} train accuracy is {:.3f}".format(clf_name, train_acc))
    
    D_initialize_arrays()
    
    D_increment1(0)
    globals.D_dummy_array1_1=globals.D_dummy_array1
    
    D_increment1(0)
    globals.D_dummy_array1_2=globals.D_dummy_array1_1
    
    D_initialize_arrays()
    globals.D_dummy_array2_1=globals.D_dummy_array2
    globals.D_dummy_array1_3=globals.D_dummy_array1
    
    D_increment2(1)
    globals.D_dummy_array2_2=globals.D_dummy_array2_1
    
    D_increment2(2)
    globals.D_dummy_array2_3=globals.D_dummy_array2_2
    
    for i in range(0,100):
        D_increment2(i)
    globals.D_dummy_array2_4=globals.D_dummy_array2_3

    with open(snakemake.output[0], 'wb') as out_file:
        pickle.dump(globals.D_dummy_array2_4)