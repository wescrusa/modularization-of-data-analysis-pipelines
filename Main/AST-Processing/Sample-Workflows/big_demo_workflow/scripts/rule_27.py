import sys
import pickle
import globals
import pandas as pd
from funcs.functions import O__get_all_data

if __name__ == '__main__':

    with open(snakemake.input[0], 'rb') as in_file:
        train = pickle.load(in_file)
    
    test = pd.read_csv(snakemake.input[1])
    print("ObjectOrientedTitanic object created")
    
    globals.O_testPassengerID=test['PassengerId']
    
    globals.O_number_of_train=train.shape[0]
    
    globals.O_y_train=train['Survived']
    
    globals.O_train=train.drop('Survived', axis=1)
    
    globals.O_test=test
    
    globals.O_all_data=O__get_all_data()
    print("Information object created")
    
    globals.S_dats=None
    print("Preprocess object created")
    print("Visualizer object created!")
    print("GridSearchHelper Created")
    
    globals.G_gridSearchCV=None
    
    globals.G_clf_and_params=list()
    with open(snakemake.output[0], 'wb') as out_file:
        pickle.dump(globals.O_testPassengerID, out_file)
    with open(snakemake.output[1], 'wb') as out_file:
        pickle.dump(globals.O_y_train, out_file)
    with open(snakemake.output[2], 'wb') as out_file:
        pickle.dump(globals.O_number_of_train, out_file)
    with open(snakemake.output[3], 'wb') as out_file:
        pickle.dump(globals.O_all_data, out_file)
    with open(snakemake.output[4], 'wb') as out_file:
        pickle.dump(globals.O_test, out_file)
    with open(snakemake.output[5], 'wb') as out_file:
        pickle.dump(globals.O_train, out_file)
    with open(snakemake.output[6], 'wb') as out_file:
        pickle.dump(globals.G_clf_and_params, out_file)
