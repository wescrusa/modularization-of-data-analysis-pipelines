rule rule_23:
    input: 'data/data/train.csv'
    output: 'data/train.pkl'
    script: '../scripts/rule_23.py'

rule rule_27:
    input: 'data/train.pkl', 'data/data/test.csv'
    output: 'data/globals.O_testPassengerID.pkl', 'data/globals.O_y_train.pkl', 'data/globals.O_number_of_train.pkl', 'data/globals.O_all_data.pkl', 'data/globals.O_test.pkl', 'data/globals.O_train.pkl', 'data/globals.G_clf_and_params.pkl'
    script: '../scripts/rule_27.py'

rule rule_42:
    input: 'data/globals.G_clf_and_params.pkl'
    output: 'data/globals.G_clf_and_params_2.pkl'
    script: '../scripts/rule_42.py'

rule rule_48:
    input: 'data/globals.G_clf_and_params_2.pkl'
    output: 'data/globals.G_clf_and_params_4.pkl'
    script: '../scripts/rule_48.py'

rule rule_54:
    input: 
    output: 'data/params_4.pkl', 'data/clf_4.pkl'
    script: '../scripts/rule_54.py'

rule rule_56:
    input: 'data/globals.O_all_data.pkl', 'data/params_4.pkl', 'data/globals.G_clf_and_params_4.pkl', 'data/clf_4.pkl'
    output: 'data/globals.G_clf_and_params_5.pkl'
    script: '../scripts/rule_56.py'

rule rule_65:
    input: 'data/globals.O_test.pkl', 'data/globals.O_train.pkl'
    output: 'data/globals.O_strategy_type.pkl', 'data/globals.S_dats_1.pkl'
    script: '../scripts/rule_65.py'

rule rule_67:
    input: 'data/globals.S_dats_1.pkl'
    output: 'data/globals.S_dats_3.pkl'
    script: '../scripts/rule_67.py'

rule rule_71:
    input: 'data/globals.S_dats_3.pkl'
    output: 'data/globals.S_dats_8.pkl', 'data/min_lengtht.pkl'
    script: '../scripts/rule_71.py'

rule rule_77:
    input: 'data/globals.S_dats_8.pkl', 'data/min_lengtht.pkl'
    output: 'data/drop_strategy_1.pkl', 'data/globals.S_dats_12.pkl'
    script: '../scripts/rule_77.py'

rule rule_83:
    input: 'data/drop_strategy_1.pkl', 'data/globals.S_dats_12.pkl'
    output: 'data/non_dummies_1.pkl', 'data/globals.S_dats_16.pkl'
    script: '../scripts/rule_83.py'

rule rule_90:
    input: 
    output: 'data/dummies_dat.pkl', 'data/columns.pkl'
    script: '../scripts/rule_90.py'

rule rule_92:
    input: 'data/globals.O_number_of_train.pkl', 'data/non_dummies_1.pkl', 'data/globals.S_dats_16.pkl', 'data/dummies_dat.pkl', 'data/columns.pkl'
    output: 'data/globals.O_all_data_1.pkl', 'data/globals.O_X_train.pkl', 'data/features.pkl'
    script: '../scripts/rule_92.py'

rule rule_98:
    input: 'data/globals.G_clf_and_params_5.pkl', 'data/globals.O_strategy_type.pkl', 'data/globals.O_testPassengerID.pkl', 'data/globals.O_y_train.pkl', 'data/globals.O_number_of_train.pkl', 'data/globals.O_all_data_1.pkl', 'data/globals.O_X_train.pkl', 'data/features.pkl'
    output: 'data/globals.G_strategy_type.pkl', 'data/globals.G_submission_id.pkl', 'data/globals.G_X_train.pkl', 'data/globals.G_y_train.pkl', 'data/globals.G_X_test.pkl', 'data/clf_and_params.pkl'
    script: '../scripts/rule_98.py'

rule rule_110:
    input: 'data/globals.G_strategy_type.pkl', 'data/globals.G_submission_id.pkl', 'data/globals.G_X_train.pkl', 'data/globals.G_y_train.pkl', 'data/globals.G_X_test.pkl', 'data/clf_and_params.pkl'
    output: 'data/models_1.pkl', 'data/globals.G_results_1.pkl'
    script: '../scripts/rule_110.py'

rule rule_125:
    input: 'data/models_1.pkl', 'data/globals.G_strategy_type.pkl', 'data/globals.G_submission_id.pkl', 'data/globals.G_results_1.pkl', 'data/globals.G_X_train.pkl', 'data/globals.G_y_train.pkl', 'data/globals.G_X_test.pkl', 'data/clf_and_params.pkl'
    output: 'data/models_2.pkl', 'data/globals.G_results_2.pkl'
    script: '../scripts/rule_125.py'

rule rule_138:
    input: 'data/models_2.pkl', 'data/globals.G_strategy_type.pkl', 'data/globals.G_submission_id.pkl', 'data/globals.G_results_2.pkl', 'data/globals.G_X_train.pkl', 'data/globals.G_y_train.pkl', 'data/globals.G_X_test.pkl', 'data/clf_and_params.pkl'
    output: 'data/models_3.pkl', 'data/globals.G_results_3.pkl'
    script: '../scripts/rule_138.py'

rule rule_151:
    input: 'data/models_3.pkl', 'data/globals.G_strategy_type.pkl', 'data/globals.G_submission_id.pkl', 'data/globals.G_results_3.pkl', 'data/globals.G_X_train.pkl', 'data/globals.G_y_train.pkl', 'data/globals.G_X_test.pkl', 'data/clf_and_params.pkl'
    output: 'data/models_4.pkl', 'data/globals.G_results_4.pkl'
    script: '../scripts/rule_151.py'

rule rule_162:
    input: 'data/globals.G_X_train.pkl', 'data/globals.G_y_train.pkl', 'data/clf_and_params.pkl'
    output: 'data/clf5.pkl', 'data/globals.G_current_clf_name_4.pkl', 'data/grid_search_clf_9.pkl'
    script: '../scripts/rule_162.py'

rule rule_169:
    input: 'data/clf5.pkl', 'data/models_4.pkl', 'data/globals.G_strategy_type.pkl', 'data/globals.G_current_clf_name_4.pkl', 'data/globals.G_submission_id.pkl', 'data/globals.G_results_4.pkl', 'data/globals.G_X_train.pkl', 'data/grid_search_clf_9.pkl', 'data/globals.G_y_train.pkl', 'data/globals.G_X_test.pkl'
    output: 'data/globals.G_results_5.pkl'
    script: '../scripts/rule_169.py'

rule rule_177:
    input: 'data/globals.G_results_5.pkl'
    output: 'data/globals.D_dummy_array2_4.pkl'
    script: '../scripts/rule_177.py'

