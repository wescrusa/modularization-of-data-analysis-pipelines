from graph_modularization import Modularizer
from Utils.workflow_utils import create_main_beginning, create_main_content, get_func_deps, create_rule
from Utils.param_utils import setup_args, setup_parser
from Utils.function_utils import is_file
import sys
import os
import ast

class WorkflowOperators(Modularizer):
    """
    WorkflowOperators extends Modularizer with rules, scripts and Snakefile collection to build a Snakemake workflow.
    """

    # <--- functions for initialising WorkflowOperators --->

    # modularize graph using graph_modularization functions
    def build_modularized_graph(self):
        print("> Modularizing operator graph...")
        if self.modType == "topological":
            self.run_modularization_top()
        else:
            self.run_modularization()
        self.extract_dictionaries()

    # read import lines from source code
    def get_import_lines(self):
        importLines = list()
        for d in self.dictionaries:
            if d["type"] == "Import" or d["type"] == "ImportFrom":
                importLines.append(d["code"])
        return importLines

    def __init__(self, filename):
        parser = setup_parser()
        args = setup_args(parser, sys.argv)
        if args.f is not None:
            sys.exit("Cannot support workflow building from JSON yet.")
        super().__init__(filename, args)
        self.build_modularized_graph()
        self.display_final_graph("Graphs/finalGraph.png")
        self.components = self.extract_components()
        self.importLines = self.get_import_lines()

    # <--- directory preparation --->

    def prep_directory(self):
        """ 
        Creates Workflow folder structure. 
        """
        originFolder = "workflow"
        mainFolder = originFolder
        # prevent overwriting existing workflows
        counter = 2
        while os.path.exists(mainFolder):
            mainFolder = originFolder + "_" + str(counter) 
            counter += 1
        # build structure
        scriptFolder = mainFolder + "/scripts"
        funcsFolder = mainFolder + "/scripts/funcs"
        rulesFolder = mainFolder + "/rules"
        resourceFolder = mainFolder + "/data"
        os.makedirs(mainFolder)
        os.makedirs(scriptFolder)
        os.makedirs(funcsFolder)
        os.makedirs(rulesFolder)
        os.makedirs(resourceFolder)
        self.mainFolder = mainFolder

    def prep_main_snakefile(self):
        """ 
        Create empty Snakefile in main workflow directory and load rule files. 
        """
        with open(self.mainFolder + "/Snakefile", 'w') as fp:
            for i in range(1,len(self.components)+1):
                fp.write("include: 'rules/rule_catalogue_{i}.smk'\n\n".format(i=i))

    def prep_catalogues(self):
        """
        Create empty rule files.
        """
        for i in range(1,len(self.components)+1):
            open(self.mainFolder + '/rules/rule_catalogue_{i}.smk'.format(i=i), "w")

    def build_directory(self):
        """
        Build entire empty workflow.
        """
        self.prep_directory()
        self.prep_main_snakefile()
        self.prep_catalogues()

    # <--- function definitions --->

    def save_functionDefs(self):
        """
        Save defined functions in functions.py script.
        """
        code = ""
        for artefact in ast.walk(self.tree):
            if isinstance(artefact, ast.FunctionDef):
                dependencies = get_func_deps(artefact.name, self.functionHandler)
                # collect imports necessary for function execution
                for dependency in [d for d in dependencies if d>=0]:
                    code = code + self.importLines[dependency] + "\n"
                # collect functionDef
                code = code + "\n" + self.extract_code(artefact) + "\n\n"
        
        with open(r'{}/scripts/funcs/functions.py'.format(self.mainFolder), 'w') as fp:
            fp.write(code)

    # <--- get imports --->

    def get_funcDef_imports(self, deps):
        """ 
        Create import line for calls to self-defined functions. 
        """
        if len(deps) == 0:
            return ""
        text = "from funcs.functions import "
        funcNames = [self.functionNames[(-1)*i-1] for i in deps]
        text += ", ".join(funcNames)
        return text + "\n"

    def read_imports(self, node):
        """ 
        Create import lines for each module dependency. 
        """
        text = "import sys\nimport pickle\n"
        ownDeps = list()
        for dependency in node["dependencies"]:
            if dependency >= 0:
                text = text + self.importLines[dependency] + "\n"
            else:
                ownDeps.append(dependency)
        text = text + self.get_funcDef_imports(ownDeps)
        return text

    # <--- generate rules and scripts --->

    def get_catalogue(self, dictionary):
        """
        Find right rule file for rule.
        """
        compNum = 0
        for i in range(0, len(self.components)):
            if dictionary["id"] in self.components[i]:
                compNum = i+1
                break
        catalogueName = self.mainFolder + "/rules/rule_catalogue_{}.smk".format(compNum)
        return catalogueName

    def create_script(self, dictionary):
        """
        Create script content for rule.
        """
        text = self.read_imports(dictionary) 
        text += create_main_beginning(dictionary)
        text += create_main_content(dictionary)
        return text

    # <--- Save to workflow folder --->

    def save_rule(self, dictionary):
        """
        Create and save rules.
        """
        file = self.get_catalogue(dictionary)
        ruleText = create_rule(dictionary)
        with open(file, "a") as fp:
            fp.write(ruleText)

    def save_script(self, dictionary):
        """
        Create and save script.
        """
        file = self.mainFolder + "/scripts/rule_{}.py".format(dictionary["id"])
        scriptText = self.create_script(dictionary)
        with open(file, "w") as fp:
            fp.write(scriptText)

    # <--- run all --->

    def create_workflow(self):
        """
        Build entire Snakemake from dictionaries.
        """
        self.build_directory()
        self.save_functionDefs()
        for d in self.finalDictionaries:
            self.save_rule(d)
            self.save_script(d)
            
# <--- Main --->

def main():
    print("<<< GENERATING SNAKEMAKE WORKFLOW >>>\n\n")
    ops = WorkflowOperators(sys.argv[1])

    print("\nExtracting Workflow...\n")
    ops.create_workflow()

if __name__ == "__main__":
    main()