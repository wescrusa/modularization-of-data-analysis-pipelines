# <--- functions to manipulate files/filenames --->
import json

def save_dict_to_file(data, filename):
    """
    Saves dictionaries to files. 
    """
    path = "JSON/" + filename
    with open(path, 'w') as f: 
        json.dump(data, f)

    # prettier format
    path = "JSON/pretty_" + filename
    j = json.dumps(data, indent=4)
    with open(path, 'w') as f:
        print(j, file=f)

def get_foldername(filename):
    """
    Assume input filename of format "../../folder1/folder2/script.py" or similar.
    We want to extract "folder1/folder2".
    """
    parts = filename.split("/")
    importantParts = [p for p in parts[:-1] if p != ".."]
    foldername = "/".join(importantParts)
    return foldername

def get_dictionaries_from_file(filename):
    """
    Read dictionaries from JSON.
    """
    with open(filename) as json_file:
        data = json.load(json_file)
    return data