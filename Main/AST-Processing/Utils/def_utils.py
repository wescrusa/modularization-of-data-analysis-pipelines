# <--- functions to collect information from functionDef nodes --->

import ast

def get_params(artefact):
    """
    Collects all parameters given to function.
    """
    params = list()
    for arg in artefact.args.args:
        params.append(arg.arg)
    return params

def get_returns(artefact):
    """
    Collect all return variable names.
    """
    returns = list()
    for node in ast.iter_child_nodes(artefact):
        if isinstance(node, ast.Return):
            for child in ast.walk(node):
                if isinstance(child, ast.Name):
                    returns.append([child.id, "Store"])
    return returns

def get_local_vars(artefact):
    """
    Collects all variable names created within the function.
    """
    params = get_params(artefact)
    locals = list()
    globals = list()
    for node in ast.walk(artefact):
        if isinstance(node, ast.Name):
            # stored before loading -> local variable
            if isinstance(node.ctx, ast.Store) and node.id not in globals:
                locals.append(node.id)
            # loaded before storing -> global variable
            if isinstance(node.ctx, ast.Load) and node.id not in locals:
                globals.append(node.id)
    return locals, globals

def get_funcDef_vars(defArtefact):
    """ Collects variable names and their usage context but sorts out local variables. """
    varNames = get_returns(defArtefact)
    locals = get_local_vars(defArtefact)[0]
    params = get_params(defArtefact)
    for node in ast.walk(defArtefact):
                if isinstance(node, ast.Name) and node.id not in locals:
                    varNames.append([node.id, type(node.ctx).__name__])
    return [params, varNames]