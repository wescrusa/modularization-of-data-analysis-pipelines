import sys
import pickle
import globals
import pandas as pd
from sklearn.preprocessing import LabelEncoder
from funcs.functions import P_drop

if __name__ == '__main__':

    with open(snakemake.input[0], 'rb') as in_file:
        drop_strategy_1 = pickle.load(in_file)
    with open(snakemake.input[1], 'rb') as in_file:
        globals.S_dats_12 = pickle.load(in_file)
    
    globals.S_dats_12 = P_drop(globals.S_dats_12, drop_strategy_1)
    globals.S_dats_13=globals.S_dats_12
    
    globals.S_dats_13 = globals.S_dats_13
    globals.S_dats_14=globals.S_dats_13
    
    labelEncoder=LabelEncoder()
    
    for column in globals.S_dats_14.columns.values:
        if 'int64'==globals.S_dats_14[column].dtype or 'float64'==globals.S_dats_14[column].dtype or 'int64'==globals.S_dats_14[column].dtype:
            continue
        labelEncoder.fit(globals.S_dats_14[column])
        globals.S_dats_14[column]=labelEncoder.transform(globals.S_dats_14[column])
    labelEncoder_1=labelEncoder
    globals.S_dats_15=globals.S_dats_14
    column_1=column
    
    globals.S_dats_15 = globals.S_dats_15
    globals.S_dats_16=globals.S_dats_15
    
    non_dummies = list()
    
    for col in globals.S_dats_16.columns.values:
        if col not in ['Pclass', 'Sex', 'Parch', 'Embarked', 'Title', 'IsAlone']:
            non_dummies.append(col)
    non_dummies_1=non_dummies
    with open(snakemake.output[0], 'wb') as out_file:
        pickle.dump(non_dummies_1, out_file)
    with open(snakemake.output[1], 'wb') as out_file:
        pickle.dump(globals.S_dats_16, out_file)
