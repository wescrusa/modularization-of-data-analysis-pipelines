import sys
import pickle
import globals
import numpy as np
import pandas as pd
from funcs.functions import O__get_train_and_test

if __name__ == '__main__':

    with open(snakemake.input[0], 'rb') as in_file:
        globals.O_number_of_train = pickle.load(in_file)
    with open(snakemake.input[1], 'rb') as in_file:
        non_dummies_1 = pickle.load(in_file)
    with open(snakemake.input[2], 'rb') as in_file:
        globals.S_dats_16 = pickle.load(in_file)
    with open(snakemake.input[3], 'rb') as in_file:
        dummies_dat = pickle.load(in_file)
    with open(snakemake.input[4], 'rb') as in_file:
        columns = pickle.load(in_file)
    
    for col in columns:
        dummies_dat.append(pd.get_dummies(globals.S_dats_16[col],prefix=col))
    col_1=col
    columns_1=columns
    dummies_dat_1=dummies_dat
    
    for non_dummy in non_dummies_1:
        dummies_dat_1.append(globals.S_dats_16[non_dummy])
    dummies_dat_2=dummies_dat_1
    non_dummies_2=non_dummies_1
    
    globals.S_dats = pd.concat(dummies_dat_2, axis=1)
    globals.S_dats_17=globals.S_dats
    
    globals.O_all_data = globals.S_dats_17
    globals.O_all_data_1=globals.O_all_data
    
    O__get_train_and_test()
    
    if None is None:
        features=globals.O_X_train.columns.values
    else:
        features=globals.O_X_train.columns.values[:None]
    with open(snakemake.output[0], 'wb') as out_file:
        pickle.dump(globals.O_all_data_1, out_file)
    with open(snakemake.output[1], 'wb') as out_file:
        pickle.dump(globals.O_X_train, out_file)
    with open(snakemake.output[2], 'wb') as out_file:
        pickle.dump(features, out_file)
